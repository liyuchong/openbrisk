/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>

#include <BRISK/include/BriskImage.h>
#include <BRISK/include/BriskMemory.h>

BriskImage *briskImageGrayCopy(const BriskImage *image)
{
    b_size_t length;
    /* alloc memory for the new image */
    BriskImage *imageCopy = (BriskImage *)BRISK_MALLOC(sizeof(BriskImage));
    BRISK_MALLOC_FAIL_CHECK(imageCopy);
    
    /* copy attributes */
    imageCopy->width = image->width;
    imageCopy->height = image->height;
    imageCopy->format = image->format;
    
    length = image->width * image->height;
    
    /* alloc memory for the pixel buffer of the new image */
    imageCopy->pixel = (b_uint8_t *)BRISK_MALLOC(sizeof(b_uint8_t) * length);
    BRISK_MALLOC_FAIL_CHECK(imageCopy->pixel);
    
    /* copy the pixels */
    BRISK_MEMCPY(imageCopy->pixel, image->pixel, length * sizeof(b_uint8_t));
    
    return imageCopy;
}

BriskImage *briskImageInitWithSize(BriskImage *image,
                                   const b_uint32_t width,
                                   const b_uint32_t height,
                                   const int format)
{
    b_size_t length = sizeof(b_uint8_t) * width * height;
    
    assert(image);

    /* copy attributes */
    image->width = width;
    image->height = height;
    image->format = format;

    /* alloc memory for pixel buffer */
    image->pixel = (b_uint8_t *)BRISK_MALLOC(length);
    BRISK_MALLOC_FAIL_CHECK(image->pixel);
    
    /* initialize the space, fill 255 */
    BRISK_MEMSET(image->pixel, 255, length);
    
    return image;
}

BriskImage *briskImageAllocWithSize(const b_uint32_t width,
                                    const b_uint32_t height,
                                    b_int_t format)
{
    BriskImage *image = (BriskImage *)BRISK_MALLOC(sizeof(BriskImage));
    BRISK_MALLOC_FAIL_CHECK(image);
    
    briskImageInitWithSize(image, width, height, format);
    
    return image;
}

void briskImageRelease(BriskImage *image)
{
    if(image)
    {
        if(image->pixel)
        {
            BRISK_MFREE(image->pixel);
            image->pixel = NULL;
        }
        BRISK_MFREE(image);
        image = NULL;
    }
}
