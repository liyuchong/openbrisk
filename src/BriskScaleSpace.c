/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <BRISK/include/BriskPrototypes.h>

/* aux */
static bool _isMax2D(const struct BriskScaleSpace *scaleSpace,
              const b_uint8_t layer,
              const b_int_t xLayer, const b_int_t yLayer);

static float _subpixel2D(const b_int_t s00, const b_int_t s01, const b_int_t s02,
                  const b_int_t s10, const b_int_t s11, const b_int_t s12,
                  const b_int_t s20, const b_int_t s21, const b_int_t s22,
                  b_float_t *deltaX, b_float_t *deltaY);

static float _refine3D(struct BriskScaleSpace *scaleSpace,
                const b_uint8_t layer,
                const b_int_t xLayer, const b_int_t yLayer,
                b_float_t *x, b_float_t *y,
                b_float_t *scale, bool *isMax);

static float _getScoreMaxAbove(const struct BriskScaleSpace *scaleSpace,
                        const b_uint8_t layer,
                        const b_int_t xLayer, const b_int_t yLayer,
                        const b_int_t threshold,
                        bool *isMax, b_float_t *dx, b_float_t *dy);

static float _getScoreMaxBelow(const struct BriskScaleSpace *scaleSpace,
                        const b_uint8_t layer,
                        const b_int_t xLayer, const b_int_t yLayer,
                        const b_int_t threshold,
                        bool *isMax, b_float_t *dx, b_float_t *dy);

struct BriskScaleSpace *briskScaleSpaceCreate(b_uint8_t octaves)
{
    struct BriskScaleSpace *briskScaleSpace =
        (struct BriskScaleSpace *)BRISK_MALLOC(sizeof(struct BriskScaleSpace));
    BRISK_MALLOC_FAIL_CHECK(briskScaleSpace);
    
    SV_ALLOC(BriskLayerContainer, briskScaleSpace->pyramid,
                        16, struct BriskLayer);
    
    if(octaves == 0)
        briskScaleSpace->layers = 1;
    else
        briskScaleSpace->layers = octaves * 2;
    
    return briskScaleSpace;
}

struct BriskScaleSpace *briskScaleSpaceDestroy(struct BriskScaleSpace *ptr)
{
    b_size_t i;
    
    if(ptr)
    {
        for(i = 0; i < SV_SIZE(ptr->pyramid); i++)
        {
            struct BriskLayer *layer = &SV_AT(ptr->pyramid, i);

            briskImageRelease(layer->image);
            briskImageRelease(layer->scores);
        }
        
        SV_RELEASE(ptr->pyramid);
        BRISK_MFREE(ptr);
        ptr = NULL;
    }
    
    return ptr;
}

struct BriskScaleSpace *briskConstructPyramid(struct BriskScaleSpace *space,
                                              const BriskImage *image)
{
    b_int_t i;
    
    assert(space && image);
    assert(SV_SIZE(space->pyramid) == 0);
    
    struct BriskLayer briskLayer = briskLayerCreate(briskImageGrayCopy(image),
                                                    1.0f, 0.0f);
    
    SV_PUSH_BACK(space->pyramid, briskLayer, struct BriskLayer);
    
    if(space->layers > 1)
    {
        struct BriskLayer briskLayer = briskLayerDerive(
                 &SV_AT(space->pyramid,
                                   SV_SIZE(space->pyramid) - 1),
                                   LAYER_TWO_THIRD_SAMPLE);
        
        SV_PUSH_BACK(space->pyramid, briskLayer, struct BriskLayer);
    }
    
    const b_int_t octaves2 = space->layers;
    
    for(i = 2; i < octaves2; i += 2)
    {
        SV_PUSH_BACK(space->pyramid,
            briskLayerDerive(&SV_AT(space->pyramid, i - 2),
                                   LAYER_HALF_SAMPLE), struct BriskLayer);
        
        SV_PUSH_BACK(space->pyramid,
            briskLayerDerive(&SV_AT(space->pyramid, i - 1),
                                   LAYER_HALF_SAMPLE), struct BriskLayer);
    }
    
    return space;
}

#define __CALCULATE_AGAST_SCORE_MATRIX(__layer, __x, __y)           \
    b_int_t s00 = getAgastScore(__layer, (__x) - 1, (__y) - 1, 1);  \
    b_int_t s10 = getAgastScore(__layer, (__x),     (__y) - 1, 1);  \
    b_int_t s20 = getAgastScore(__layer, (__x) + 1, (__y) - 1, 1);  \
    b_int_t s21 = getAgastScore(__layer, (__x) + 1, (__y),     1);  \
    b_int_t s11 = getAgastScore(__layer, (__x),     (__y),     1);  \
    b_int_t s01 = getAgastScore(__layer, (__x) - 1, (__y),     1);  \
    b_int_t s02 = getAgastScore(__layer, (__x) - 1, (__y) + 1, 1);  \
    b_int_t s12 = getAgastScore(__layer, (__x),     (__y) + 1, 1);  \
    b_int_t s22 = getAgastScore(__layer, (__x) + 1, (__y) + 1, 1);

FeaturePointContainer *briskDetectFeaturePoints(struct BriskScaleSpace *space,
                                                const b_uint16_t threshold)
{
    b_size_t i, j, n;
    b_float_t x, y, scale, score;
    
    space->threshold     = threshold;
    space->safeThreshold = space->threshold * 1.0f/*safetyFactor_*/;

    FeaturePointContainer *featurePoints;
    SV_ALLOC(FeaturePointContainer, featurePoints, 32, FeaturePoint);
    
    SV_DECLARE(PointsContainer, BriskPoint);
    SV_DECLARE(AgastPoints, PointsContainer *);
    
    AgastPoints *agastPoints;
    SV_ALLOC(AgastPoints, agastPoints,
                        space->layers, PointsContainer *);
    
    /* go through the octaves and intra layers 
     * and calculate fast corner scores */
    for(i = 0; i < space->layers; i++)
    {
        int length;
        PointsContainer *points;
        BriskPoint *ptr =
            getAgastPoints(&SV_AT(space->pyramid, i),
                           space->safeThreshold, &length);
        
        SV_ALLOC(PointsContainer, points, length, BriskPoint);
        for(j = 0; j < length; j++)
            SV_PUSH_BACK(points, ptr[j], BriskPoint);
        
        SV_PUSH_BACK(agastPoints, points, PointsContainer *);
    }
    
#define __BRISK_DETECT_FEATURE_POINTS_CLEANUP       \
do {                                                \
    for(i = 0; i < SV_SIZE(agastPoints); i++)       \
        SV_RELEASE(SV_AT(agastPoints, i));          \
    SV_RELEASE(agastPoints);                        \
} while(0)
    
    if(space->layers == 1)
    {
        const b_size_t num =
            SV_SIZE(SV_AT(agastPoints, 0));
        
        for(n = 0; n < num; n++)
        {
            const BriskPoint point =
                SV_AT(SV_AT(agastPoints, 0), n);
            
            /* first check if it is a maximum */
            if(!_isMax2D(space, 0, point.x, point.y))
                continue;
            
            /* let's do the subpixel and float scale refinement: */
            struct BriskLayer *layer =
                &SV_AT(space->pyramid, 0);
            
            __CALCULATE_AGAST_SCORE_MATRIX(layer, point.x, point.y);
            
            b_float_t deltaX, deltaY;
            b_float_t max = _subpixel2D(s00, s01, s02,
                                        s10, s11, s12,
                                        s20, s21, s22,
                                        &deltaX, &deltaY);
            
            FeaturePoint featurePoint = featurePointMake(
                (b_float_t)point.x + deltaX, (b_float_t)point.y + deltaY,
                BRISK_BASIC_SIZE, -1.0f, max, 0.0f);
            
            SV_PUSH_BACK(featurePoints, featurePoint, FeaturePoint);
        }
        
        __BRISK_DETECT_FEATURE_POINTS_CLEANUP;
        
        return featurePoints;
    }
    
    for(i = 0; i < space->layers; i++)
    {
        struct BriskLayer *layer = &SV_AT(space->pyramid, i);
        const b_size_t num =
            SV_SIZE(SV_AT(agastPoints, i));
        
        if(i == space->layers - 1)
        {
            for(n = 0; n < num; n++)
            {
                const BriskPoint point =
                    SV_AT(SV_AT(agastPoints, i), n);

                if (!_isMax2D(space, i, point.x, point.y))
                    continue;
                
                bool ismax;
                b_float_t dx, dy;
                _getScoreMaxBelow(space, i, point.x, point.y,
                                 getAgastScore(layer, point.x, point.y,
                                               space->safeThreshold),
                                 &ismax, &dx, &dy);
                
                if(!ismax)
                    continue;
                
                __CALCULATE_AGAST_SCORE_MATRIX(layer, point.x, point.y);
                
                b_float_t deltaX, deltaY;
                b_float_t max = _subpixel2D(s00, s01, s02,
                                            s10, s11, s12,
                                            s20, s21, s22,
                                            &deltaX, &deltaY);

                b_float_t xv = point.x + deltaX * layer->scale + layer->offset;
                b_float_t yv = point.y + deltaY * layer->scale + layer->offset;

                FeaturePoint featurePoint = featurePointMake(xv, yv,
                    BRISK_BASIC_SIZE * layer->scale,
                    -1.0f, max, (b_int_t)i);
                
                SV_PUSH_BACK(featurePoints, featurePoint, FeaturePoint);
            }
        }
        else
        {
            /* not the last layer: */
            for(n = 0; n < num; n++)
            {
                const BriskPoint point = SV_AT(SV_AT(agastPoints, i), n);
                
                /* first check if it is a maximum: */
                if(!_isMax2D(space, i, point.x, point.y))
                    continue;
                
                /* let's do the subpixel and float scale refinement: */
                bool ismax;
                score = _refine3D(space, i,point.x, point.y,
                                 &x, &y, &scale, &ismax);
                if(!ismax)
                    continue;
                
                /* finally store the detected featurePoint: */
                if(score > (b_float_t)(space->threshold))
                {
                    FeaturePoint featurePoint = featurePointMake(x, y,
                        BRISK_BASIC_SIZE * scale, -1.0f,
                        score, (b_int_t)i);
                    
                    SV_PUSH_BACK(featurePoints, featurePoint, FeaturePoint);
                }
            }
        }
    }
    
    __BRISK_DETECT_FEATURE_POINTS_CLEANUP;

#undef __BRISK_DETECT_FEATURE_POINTS_CLEANUP
    
    return featurePoints;
}

/* 1D (scale axis) refinement: */
/* around octave */
float refine1D(const b_float_t s_05, const b_float_t s0, const b_float_t s05,
               b_float_t *max)
{
    b_int_t i_05 = (b_int_t)(1024.0f * s_05 + 0.5f);
    b_int_t i0   = (b_int_t)(1024.0f * s0   + 0.5f);
    b_int_t i05  = (b_int_t)(1024.0f * s05  + 0.5f);
    
    /********************************
     *  16.0000  -24.0000    8.0000 *
     * -40.0000   54.0000  -14.0000 *
     *  24.0000  -27.0000    6.0000 *
     ********************************/
    
    b_int_t threeA = 16 * i_05 - 24 * i0 + 8 * i05;
    
    if(threeA >= 0)
    {
        if(s0 >= s_05 && s0 >= s05)
        {
            *max = s0;
            return 1.0f;
        }
        if(s_05 >= s0 && s_05 >= s05)
        {
            *max = s_05;
            return 0.75f;
        }
        if(s05 >= s0 && s05 >= s_05)
        {
            *max = s05;
            return 1.5f;
        }
    }
    
    b_int_t three_b = -40 * i_05 + 54 * i0 - 14 * i05;
    
    b_float_t result = -(b_float_t)(three_b) / (b_float_t)(2 * threeA);
    
    if(result < 0.75f)
        result = 0.75f;
    else if(result > 1.5f)
        result = 1.5f;
    
    b_int_t three_c = 24 * i_05 -27 * i0 + 6 * i05;
    
    *max = (b_float_t)(three_c) + (b_float_t)(threeA) * result
    * result+(b_float_t)(three_b) * result;
    *max /= 3072.0f;
    
    return result;
}

float refine1D_1(const b_float_t s_05,
                 const b_float_t s0, const b_float_t s05, b_float_t *max)
{
    b_int_t i_05 = (b_int_t)(1024.0f * s_05 + 0.5f);
    b_int_t i0   = (b_int_t)(1024.0f * s0   + 0.5f);
    b_int_t i05  = (b_int_t)(1024.0f * s05  + 0.5f);
    
    /********************************
     *  4.5000   -9.0000    4.5000  *
     * -10.5000   18.0000   -7.5000 *
     *  6.0000   -8.0000    3.0000  *
     ********************************/
    
#define _TWO_THIRD  0.6666666666666666666666666667f
#define _FOUR_THIRD 1.3333333333333333333333333333f
    
    b_int_t twoA = 9 * i_05 - 18 * i0 + 9 * i05;
    
    /* second derivative must be negative: */
    if(twoA >= 0)
    {
        if(s0 >= s_05 && s0 >= s05)
        {
            *max = s0;
            return 1.0f;
        }
        if(s_05 >= s0 && s_05 >= s05)
        {
            *max = s_05;
            return _TWO_THIRD;
        }
        if(s05 >= s0 && s05 >= s_05)
        {
            *max = s05;
            return _FOUR_THIRD;
        }
    }
    
    b_int_t twoB = -21 * i_05 + 36 * i0 - 15 * i05;
    
    /* calculate max location: */
    b_float_t result = -(b_float_t)(twoB) / (b_float_t)(2 * twoA);
    
    /* saturate and return */
    if(result<_TWO_THIRD)
        result= _TWO_THIRD;
    else if(result>_FOUR_THIRD)
        result= _FOUR_THIRD;
    
    b_int_t twoC = 12 * i_05 - 16 * i0 +6 * i05;
    
    *max = (b_float_t)(twoC) + (b_float_t)(twoA) * result * result
    + (b_float_t)(twoB) * result;
    *max /= 2048.0f;
    
    return result;
    
#undef _TWO_THIRD
#undef _FOUR_THIRD
}

float refine1D_2(const b_float_t s_05, const b_float_t s0, const b_float_t s05,
                 b_float_t *max)
{
    b_int_t i_05 = (b_int_t)(1024.0f * s_05 + 0.5f);
    b_int_t i0   = (b_int_t)(1024.0f * s0   + 0.5f);
    b_int_t i05  = (b_int_t)(1024.0f * s05  + 0.5f);
    
    /*********************************
     *   18.0000  -30.0000   12.0000 *
     *  -45.0000   65.0000  -20.0000 *
     *   27.0000  -30.0000    8.0000 *
     *********************************/
    
    b_int_t a = 2 * i_05 - 4 * i0 + 2 * i05;
    /* second derivative must be negative: */
    if(a >= 0)
    {
        if(s0 >= s_05 && s0 >= s05)
        {
            *max = s0;
            return 1.0f;
        }
        if(s_05 >= s0 && s_05 >= s05)
        {
            *max = s_05;
            return 0.7f;
        }
        if(s05 >= s0 && s05 >= s_05)
        {
            *max = s05;
            return 1.5f;
        }
    }
    
    b_int_t b = -5 * i_05 + 8 * i0 - 3 * i05;
    /* calculate max location: */
    b_float_t result = -(b_float_t)b / (2.0f * a);
    /* saturate and return */
    
    if(result < 0.7f)
        result = 0.7f;
    else if(result > 1.5f)
        result = 1.5f;
    
    int c = 3 * i_05 - 3 * i0 + 1 * i05;
    
    *max = (b_float_t)c + (b_float_t)a * result * result + (b_float_t)b * result;
    *max /= 1024;
    
    return result;
}

float _refine3D(struct BriskScaleSpace *scaleSpace, const b_uint8_t layer,
                const b_int_t xLayer, const b_int_t yLayer,
                b_float_t *x, b_float_t *y, b_float_t *scale, bool *isMax)
{
    *isMax = true;
    
    struct BriskLayer *thisLayer = &SV_AT(scaleSpace->pyramid, layer);
    const b_int_t center = getAgastScore(thisLayer, xLayer,yLayer,1);
    
    b_float_t deltaAboveX, deltaAboveY;
    b_float_t maxAbove = _getScoreMaxAbove(scaleSpace, layer,xLayer, yLayer,
                                           center, isMax,
                                           &deltaAboveX, &deltaAboveY);
    if(!isMax)
        return 0.0f;
    
    b_float_t max;
    
    if(layer % 2 == 0)
    {
        /* octave */
        b_float_t deltaXBelow, deltaYBelow;
        b_float_t maxBelowFloat;
        b_uint8_t maxBelowCount = 0;
        
        if(layer == 0)
        {
            struct BriskLayer *l = &SV_AT(scaleSpace->pyramid, 0);
            
            b_int_t s00 = getAgastScore_5_8(l, xLayer - 1, yLayer - 1, 1);
            maxBelowCount = s00;
            
            b_int_t s10 = getAgastScore_5_8(l, xLayer,     yLayer - 1, 1);
            if(s10 > maxBelowCount) maxBelowCount = s10;
            
            b_int_t s20 = getAgastScore_5_8(l, xLayer + 1, yLayer - 1, 1);
            if(s20 > maxBelowCount) maxBelowCount = s20;
            
            b_int_t s21 = getAgastScore_5_8(l, xLayer + 1, yLayer,     1);
            if(s21 > maxBelowCount) maxBelowCount = s21;
            
            b_int_t s11 = getAgastScore_5_8(l, xLayer,     yLayer,     1);
            if(s11 > maxBelowCount) maxBelowCount = s11;
            
            b_int_t s01 = getAgastScore_5_8(l, xLayer - 1, yLayer,     1);
            if(s01 > maxBelowCount) maxBelowCount = s01;
            
            b_int_t s02 = getAgastScore_5_8(l, xLayer - 1, yLayer + 1, 1);
            if(s02 > maxBelowCount) maxBelowCount = s02;
            
            b_int_t s12 = getAgastScore_5_8(l, xLayer,     yLayer + 1, 1);
            if(s12 > maxBelowCount) maxBelowCount = s12;
            
            b_int_t s22 = getAgastScore_5_8(l, xLayer + 1, yLayer + 1, 1);
            if(s22 > maxBelowCount) maxBelowCount = s22;
            
            maxBelowFloat = maxBelowCount;
        }
        else
        {
            maxBelowFloat = _getScoreMaxBelow(scaleSpace,layer,xLayer, yLayer,
                                              center, isMax,
                                              &deltaXBelow, &deltaYBelow);
            if(!isMax)
                return 0;
        }
        
        __CALCULATE_AGAST_SCORE_MATRIX(thisLayer, xLayer, yLayer);
        
        b_float_t deltaLayerX, deltaLayerY;
        b_float_t maxLayer = _subpixel2D(s00, s01, s02,
                                         s10, s11, s12,
                                         s20, s21, s22,
                                         &deltaLayerX, &deltaLayerY);
        
        if(layer == 0)
        {
            *scale = refine1D_2(maxBelowFloat,
                                fmaxf((b_float_t)(center),
                                      maxLayer), maxAbove, &max);
        }
        else
        {
            *scale = refine1D(maxBelowFloat,
                              fmaxf((b_float_t)(center), maxLayer),
                              maxAbove,&max);
        }
        
        if(*scale > 1.0)
        {
            /* interpolate the position: */
            const b_float_t r0 = (1.5f - *scale) / 0.5f;
            const b_float_t r1 = 1.0f - r0;
            
            *x = (r0 * deltaLayerX + r1 * deltaAboveX + (b_float_t)xLayer)
            * thisLayer->scale + thisLayer->offset;
            
            *y = (r0 * deltaLayerY + r1 * deltaAboveY + (b_float_t)yLayer)
            * thisLayer->scale + thisLayer->offset;
        }
        else
        {
            if(layer == 0)
            {
                const b_float_t r0 = (*scale - 0.5f) / 0.5f;
                const b_float_t r_1 = 1.0f - r0;
                *x = r0 * deltaLayerX + r_1 * deltaXBelow + (b_float_t)xLayer;
                *y = r0 * deltaLayerY + r_1 * deltaYBelow + (b_float_t)yLayer;
            }
            else
            {
                const b_float_t r0 = (*scale - 0.75f) / 0.25f;
                const b_float_t r_1 = 1.0f - r0;
                *x = (r0 * deltaLayerX + r_1 * deltaXBelow + (b_float_t)xLayer)
                * thisLayer->scale+thisLayer->offset;
                *y = (r0 * deltaLayerY + r_1 * deltaYBelow + (b_float_t)yLayer)
                * thisLayer->scale+thisLayer->offset;
            }
        }
    }
    else
    {
        b_float_t deltaBelowX, deltaBelowY;
        b_float_t maxBelow = _getScoreMaxBelow(scaleSpace,layer,xLayer, yLayer,
                                               center, isMax,
                                               &deltaBelowX, &deltaBelowY);
        if(!isMax)
            return 0.0f;
        
        __CALCULATE_AGAST_SCORE_MATRIX(thisLayer, xLayer, yLayer);
        
        b_float_t deltalayerX, deltalayerY;
        b_float_t maxLayer = _subpixel2D(s00, s01, s02,
                                         s10, s11, s12,
                                         s20, s21, s22,
                                         &deltalayerX, &deltalayerY);
        *scale = refine1D_1(maxBelow,
                            fmaxf(center, maxLayer),
                            maxAbove, &max);
        if(*scale > 1.0f)
        {
            const b_float_t r0 = 4.0f - *scale * 3.0f;
            const b_float_t r1 = 1.0f - r0;
            
            *x = (r0 * deltalayerX + r1 * deltaAboveX + (b_float_t)(xLayer))
            * thisLayer->scale + thisLayer->offset;
            *y = (r0 * deltalayerY + r1 * deltaAboveY + (b_float_t)(yLayer))
            * thisLayer->scale + thisLayer->offset;
        }
        else
        {
            const b_float_t r0 = *scale * 3.0f - 2.0f;
            const b_float_t r1 = 1.0f - r0;
            *x = (r0 * deltalayerX + r1 * deltaBelowX + (b_float_t)(xLayer))
            * thisLayer->scale + thisLayer->offset;
            *y = (r0 * deltalayerY + r1 * deltaBelowY + (b_float_t)(yLayer))
            * thisLayer->scale + thisLayer->offset;
        }
    }
    
    *scale *= thisLayer->scale;
    
    return max;
}

/* interpolated score access with recalculation when needed: */
int getScoreAbove(const struct BriskScaleSpace *this, const b_uint8_t layer,
                  const b_int_t xLayer, const b_int_t yLayer)
{
    struct BriskLayer *l = &SV_AT(this->pyramid, layer + 1);
    
    assert(layer < this->layers - 1);
    
    if(layer % 2 == 0)
    {
        const b_int_t sixthsX = 4 * xLayer - 1;
        const b_int_t sixthsY = 4 * yLayer - 1;
        
        const b_int_t xAbove = sixthsX / 6;
        const b_int_t yAbove = sixthsY / 6;
        
        const b_int_t rx = (sixthsX % 6);
        const b_int_t ry = (sixthsY % 6);
        
        const b_int_t ry1 = 6 - ry;
        const b_int_t rx1 = 6 - rx;
        
        b_uint8_t score = 0xFF &
        ((rx1 * ry1 * getAgastScore(l, xAbove,     yAbove,     1)
          + rx  * ry1 * getAgastScore(l, xAbove + 1, yAbove,     1)
          + rx1 * ry  * getAgastScore(l, xAbove,     yAbove + 1, 1)
          + rx  * ry  * getAgastScore(l, xAbove + 1, yAbove + 1, 1)
          + 18) / 36);
        
        return score;
    }
    else
    {
        const b_int_t eighthsX = 6 * xLayer - 1;
        const b_int_t eighthsY = 6 * yLayer - 1;
        
        const b_int_t xAbove = eighthsX / 8;
        const b_int_t yAbove = eighthsY / 8;
        
        const b_int_t rx = (eighthsX % 8);
        const b_int_t ry = (eighthsY % 8);
        
        const b_int_t rx1 = 8 - rx;
        const b_int_t ry1 = 8 - ry;
        
        b_uint8_t score = 0xff &
        ((rx1 * ry1 * getAgastScore(l, xAbove,     yAbove,     1)
          + rx  * ry1 * getAgastScore(l, xAbove + 1, yAbove,     1)
          + rx1 * ry  * getAgastScore(l, xAbove,     yAbove + 1, 1)
          + rx  * ry  * getAgastScore(l, xAbove + 1, yAbove + 1, 1)
          + 32) / 64);
        
        return score;
    }
}

int getScoreBelow(const struct BriskScaleSpace *this, const b_uint8_t layer,
                  const b_int_t xLayer, const b_int_t yLayer)
{
    b_float_t xf, yf, offs, area;
    b_float_t x_1, x1, y_1, y1, r_x_1, r_y_1, r_x1, r_y1;
    
    b_int_t xLeft, yTop, xRight, yBottom;
    b_int_t sixthX, sixthY, quarterX, quarterY, scaling, scaling2;
    b_int_t rx_1i, ry_1i, rx1i, ry1i, A, B, C, D, dx, dy;
    b_int_t result, x, y;
    
    struct BriskLayer *l = &SV_AT(this->pyramid, layer - 1);
    
    if(layer % 2 == 0)
    {
        /* octave */
        sixthX = 8 * xLayer + 1;
        sixthY = 8 * yLayer + 1;
        
        xf = (b_float_t)(sixthX) / 6.0f;
        yf = (b_float_t)(sixthY) / 6.0f;
        
        /* scaling: */
        offs = 2.0f / 3.0f;
        area = 4.0f * offs * offs;
        scaling  = 4194304.0f / area;
        scaling2 = (b_float_t)(scaling) * area;
    }
    else
    {
        quarterX = 6 * xLayer + 1;
        quarterY = 6 * yLayer + 1;
        
        xf = (b_float_t)(quarterX) / 4.0f;
        yf = (b_float_t)(quarterY) / 4.0f;
        
        /* scaling: */
        offs = 3.0f / 4.0f;
        area = 4.0f * offs * offs;
        scaling  = 4194304.0f / area;
        scaling2 = (b_float_t)(scaling) * area;
    }
    
    /* calculate borders */
    x_1 = xf-offs; x1  = xf+offs;
    y_1 = yf-offs; y1  = yf+offs;
    
    xLeft   = (b_int_t)(x_1 + 0.5f);
    yTop    = (b_int_t)(y_1 + 0.5f);
    xRight  = (b_int_t)(x1  + 0.5f);
    yBottom = (b_int_t)(y1  + 0.5f);
    
    /* overlap area - multiplication factors: */
    r_x_1 = (b_float_t)(xLeft) - x_1 + 0.5f;
    r_y_1 = (b_float_t)(yTop) - y_1 + 0.5f;
    
    r_x1 = x1 - (b_float_t)(xRight) + 0.5f;
    r_y1 = y1 - (b_float_t)(yBottom) + 0.5f;
    
    dx = xRight  - xLeft - 1; dy = yBottom - yTop - 1;
    
    A = (r_x_1 * r_y_1) * scaling; B = (r_x1  * r_y_1) * scaling;
    C = (r_x1  * r_y1)  * scaling; D = (r_x_1 * r_y1)  * scaling;
    
    rx_1i = r_x_1 * scaling; ry_1i = r_y_1 * scaling;
    rx1i  = r_x1  * scaling; ry1i  = r_y1  * scaling;
    
    /* first row: */
    result = A * (b_int_t)(getAgastScore(l, xLeft, yTop, 1));
    
    for(x = 1; x <= dx; x++)
        result += ry_1i * (b_int_t)(getAgastScore(l, xLeft + x, yTop, 1));
    
    result += B * (b_int_t)(getAgastScore(l, xLeft + dx + 1,yTop,1));
    
    /* middle ones: */
    for(y = 1; y <= dy; y++)
    {
        result += rx_1i * (b_int_t)(getAgastScore(l, xLeft, yTop + y, 1));
        
        for(x = 1; x <= dx; x++)
            result += (getAgastScore(l, xLeft + x, yTop + y, 1)) * scaling;
        
        result += rx1i * (getAgastScore(l, xLeft + dx + 1, yTop + y, 1));
    }
    
    /* last row: */
    result += D * (b_int_t)(getAgastScore(l, xLeft, yTop + dy + 1, 1));
    for(x = 1; x <= dx; x++)
        result += ry1i * (getAgastScore(l, xLeft + x, yTop + dy + 1, 1));
    
    result += C * (b_int_t)(getAgastScore(l, xLeft + dx + 1, yTop + dy + 1, 1));
    
    return ((result + scaling2 / 2) / scaling2);
}

/* return the maximum of score patches above or below */
float _getScoreMaxAbove(const struct BriskScaleSpace *scaleSpace,
                        const b_uint8_t layer,
                        const b_int_t xLayer, const b_int_t yLayer,
                        const b_int_t threshold, bool *isMax,
                        b_float_t *dx, b_float_t *dy)
{
    *isMax = false;
    
    /* relevant floating point coordinates */
    b_float_t x_1, x1, y_1, y1;
    b_int_t x, y;
    
    assert(layer + 1 < scaleSpace->layers);
    
    struct BriskLayer *layerAbove = &SV_AT(scaleSpace->pyramid, layer + 1);
    
    if(layer % 2 == 0)
    {
        x_1 = (b_float_t)(4 * xLayer - 1 - 2) / 6.0f;
        x1  = (b_float_t)(4 * xLayer - 1 + 2) / 6.0f;
        y_1 = (b_float_t)(4 * yLayer - 1 - 2) / 6.0f;
        y1  = (b_float_t)(4 * yLayer - 1 + 2) / 6.0f;
    }
    else
    {
        x_1 = (b_float_t)(6 * xLayer - 1 - 3) / 8.0f;
        x1  = (b_float_t)(6 * xLayer - 1 + 3) / 8.0f;
        y_1 = (b_float_t)(6 * yLayer - 1 - 3) / 8.0f;
        y1  = (b_float_t)(6 * yLayer - 1 + 3) / 8.0f;
    }
    
    
    b_int_t maxX = x_1 + 1;
    b_int_t maxY = y_1 + 1;
    b_float_t tmpMax;
    b_float_t max = getAgastScore(layerAbove, x_1, y_1, 1);
    
    if(max > threshold)
        return 0;
    
    for(x = x_1 + 1; x <= (b_int_t)x1; x++)
    {
        tmpMax = getAgastScore(layerAbove, (b_float_t)x, y_1, 1);
        
        if(tmpMax > threshold)
            return 0;
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = x;
        }
    }
    
    tmpMax = getAgastScore(layerAbove, x1, y_1, 1);
    
    if(tmpMax > threshold)
        return 0;
    
    if(tmpMax > max)
    {
        max = tmpMax;
        maxX = (b_int_t)x1;
    }
    
    for(y = y_1 + 1; y <= (b_int_t)y1; y++)
    {
        tmpMax = getAgastScore(layerAbove, x_1, (b_float_t)y, 1);
        
        if(tmpMax > threshold)
            return 0;
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = (b_int_t)(x_1 + 1);
            maxY = y;
        }
        
        for(x = x_1 + 1; x <= (b_int_t)x1; x++)
        {
            tmpMax = getAgastScore(layerAbove, x, y, 1);
            
            if(tmpMax > threshold)
                return 0;
            
            if(tmpMax > max)
            {
                max = tmpMax;
                maxX = x;
                maxY = y;
            }
        }
        
        tmpMax = getAgastScore(layerAbove, x1, (b_float_t)y, 1);
        
        if(tmpMax > threshold)
            return 0;
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = (b_int_t)x1;
            maxY = y;
        }
    }
    
    tmpMax = getAgastScore(layerAbove, x_1, y1, 1);
    
    if(tmpMax > max)
    {
        max = tmpMax;
        maxX = (b_int_t)(x_1 + 1);
        maxY = (b_int_t)y1;
    }
    
    for(x = x_1 + 1; x <= (b_int_t)x1; x++)
    {
        tmpMax = getAgastScore(layerAbove, (b_float_t)x, y1, 1);
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = x;
            maxY = (b_int_t)y1;
        }
    }
    
    tmpMax = getAgastScore(layerAbove, x1, y1 ,1);
    
    if(tmpMax > max)
    {
        max = tmpMax;
        maxX = (b_int_t)x1;
        maxY = (b_int_t)y1;
    }
    
    __CALCULATE_AGAST_SCORE_MATRIX(layerAbove, maxX, maxY);
    
    b_float_t dx1, dy1;
    b_float_t refinedMax = _subpixel2D(s00, s01, s02,
                                       s10, s11, s12,
                                       s20, s21, s22,
                                       &dx1, &dy1);
    
    /* calculate dx/dy in above coordinates */
    b_float_t real_x = (b_float_t)(maxX) + dx1;
    b_float_t real_y = (b_float_t)(maxY) + dy1;
    
    if(layer % 2 == 0)
    {
        *dx = (real_x * 6.0f + 1.0f) / 4.0f - (b_float_t)xLayer;
        *dy = (real_y * 6.0f + 1.0f) / 4.0f - (b_float_t)yLayer;
    }
    else
    {
        *dx = (real_x * 8.0f + 1.0f) / 6.0f - (b_float_t)xLayer;
        *dy = (real_y * 8.0f + 1.0f) / 6.0f - (b_float_t)yLayer;
    }
    
    bool returnRefined = true;
    
    if(*dx > 1.0f)
    {
        *dx = 1.0f;
        returnRefined = false;
    }
    if(*dx < -1.0f)
    {
        *dx = -1.0f;
        returnRefined = false;
    }
    if(*dy > 1.0f)
    {
        *dy = 1.0f;
        returnRefined = false;
    }
    if(*dy < -1.0f)
    {
        *dy = -1.0f;
        returnRefined = false;
    }
    
    *isMax = true;
    
    if(returnRefined)
        return fmaxf(refinedMax, max);
    
    return max;
}


float _subpixel2D(const b_int_t s00, const b_int_t s01, const b_int_t s02,
                  const b_int_t s10, const b_int_t s11, const b_int_t s12,
                  const b_int_t s20, const b_int_t s21, const b_int_t s22,
                  b_float_t *deltaX, b_float_t *deltaY)
{
    
    b_int_t tmp1   = s00 + s02 - 2 * s11 + s20 + s22;
    b_int_t coeff1 = 3 * (tmp1 + s01 - ((s10 + s12) << 1) + s21);
    b_int_t coeff2 = 3 * (tmp1 - ((s01 + s21) << 1) + s10 + s12 );
    b_int_t tmp2   = s02 - s20;
    b_int_t tmp3   = (s00 + tmp2 - s22);
    b_int_t tmp4   = tmp3 -2 * tmp2;
    b_int_t coeff3 = -3 * (tmp3 + s01 - s21);
    b_int_t coeff4 = -3 * (tmp4 + s10 - s12);
    b_int_t coeff5 = (s00 - s02 - s20 + s22) << 2;
    b_int_t coeff6 = -(s00  + s02 - ((s10 + s01 + s12 + s21) << 1)
                       - 5*s11  + s20  + s22) << 1;
    
    b_int_t H_det = 4 * coeff1 * coeff2 - coeff5 * coeff5;
    
    if(H_det == 0)
    {
        *deltaX = 0.0f;
        *deltaY = 0.0f;
        return coeff6 / 18.0f;
    }
    
    if(!(H_det > 0 && coeff1 < 0))
    {
        /* The maximum must be at the one of the 4 patch corners. */
        b_int_t tmpMax = coeff3 + coeff4 + coeff5;
        
        *deltaX = 1.0f;
        *deltaY = 1.0f;
        
        b_int_t tmp = -coeff3 + coeff4 - coeff5;
        
        if(tmp > tmpMax)
        {
            tmpMax = tmp;
            *deltaX = -1.0f; *deltaY = 1.0f;
        }
        
        tmp = coeff3 - coeff4 - coeff5;
        
        if(tmp > tmpMax)
        {
            tmpMax = tmp;
            *deltaX = 1.0f; *deltaY = -1.0f;
        }
        
        tmp = -coeff3 - coeff4 + coeff5;
        
        if(tmp > tmpMax)
        {
            tmpMax = tmp;
            *deltaX = -1.0f; *deltaY = -1.0f;
        }
        
        return (tmpMax + coeff1 + coeff2 + coeff6) / 18.0f;
    }
    
    *deltaX = (b_float_t)(2 * coeff2 * coeff3 - coeff4 * coeff5)
    / (b_float_t)(-H_det);
    *deltaY = (b_float_t)(2 * coeff1 * coeff4 - coeff3 * coeff5)
    / (b_float_t)(-H_det);
    
    bool tx = false, tx_ = false, ty = false, ty_ = false;
    
    if(*deltaX > 1.0f)
        tx = true;
    else if(*deltaX < -1.0f)
        tx_ = true;
    
    if(*deltaY > 1.0f)
        ty = true;
    if(*deltaY < -1.0f)
        ty_ = true;
    
    if(tx || tx_|| ty || ty_)
    {
        /* get two candidates */
        b_float_t delta_x1 = 0.0f, delta_x2 = 0.0f;
        b_float_t delta_y1 = 0.0f, delta_y2 = 0.0f;
        
        if(tx)
        {
            delta_x1 = 1.0f;
            delta_y1 = -(b_float_t)(coeff4 + coeff5) / (b_float_t)(2 * coeff2);
            
            if(delta_y1 > 1.0f)
                delta_y1 = 1.0f;
            else if(delta_y1 < -1.0f)
                delta_y1 = -1.0f;
        }
        else if(tx_)
        {
            delta_x1 = -1.0f;
            delta_y1 = -(b_float_t)(coeff4 - coeff5) / (b_float_t)(2 * coeff2);
            
            if(delta_y1 > 1.0f)
                delta_y1 = 1.0f;
            else if(delta_y1 < -1.0f)
                delta_y1 = -1.0f;
        }
        
        if(ty)
        {
            delta_y2 = 1.0f;
            delta_x2 = -(b_float_t)(coeff3 + coeff5) / (b_float_t)(2 * coeff1);
            
            if(delta_x2 > 1.0f)
                delta_x2 = 1.0f;
            else if (delta_x2 < -1.0f)
                delta_x2 = -1.0f;
        }
        else if(ty_)
        {
            delta_y2 = -1.0f;
            delta_x2 = -(b_float_t)(coeff3 - coeff5) / (b_float_t)(2 * coeff1);
            
            if(delta_x2 > 1.0f)
                delta_x2 = 1.0f;
            else if (delta_x2 < -1.0f)
                delta_x2 = -1.0f;
        }
        
        b_float_t max1 =
        (coeff1 * delta_x1 * delta_x1 + coeff2 * delta_y1 * delta_y1
         + coeff3 * delta_x1 + coeff4 * delta_y1
         + coeff5 * delta_x1 * delta_y1
         +coeff6) / 18.0f;
        
        b_float_t max2 =
        (coeff1 * delta_x2 * delta_x2 + coeff2 * delta_y2 * delta_y2
         + coeff3 * delta_x2 + coeff4 * delta_y2
         + coeff5 * delta_x2 * delta_y2
         + coeff6) / 18.0f;
        
        if(max1 > max2)
        {
            *deltaX = delta_x1;
            *deltaY = delta_x1;
            return max1;
        }
        else
        {
            *deltaX = delta_x2;
            *deltaY = delta_x2;
            return max2;
        }
    }
    
    return (coeff1 * (*deltaX) * (*deltaX) + coeff2 * (*deltaY) * (*deltaY)
            + coeff3 * (*deltaX) + coeff4 * (*deltaY)
            + coeff5 * (*deltaX) * (*deltaY)
            + coeff6) / 18.0f;
}

/* nonmax suppression: */
bool _isMax2D(const struct BriskScaleSpace *scaleSpace, const b_uint8_t layer,
              const b_int_t xLayer, const b_int_t yLayer)
{
    const BriskImage *scores = SV_AT(scaleSpace->pyramid, layer).scores;
    
    const b_int_t scoresCols = scores->width;
    b_uint8_t *data = scores->pixel + yLayer * scoresCols + xLayer;
    
    /* decision tree: */
    const b_uint8_t center = *data;
    data--;
    
    const b_uint8_t s_10 = *data;
    if(center < s_10)    return false;
    data += 2;
    
    const b_uint8_t s10 = *data;
    if(center < s10)     return false;
    data -= (scoresCols + 1);
    
    const b_uint8_t s0_1 = *data;
    if(center < s0_1)    return false;
    data += 2 * scoresCols;
    
    const b_uint8_t s01 = *data;
    if(center < s01)     return false;
    data--;
    
    const b_uint8_t s_11 = *data;
    if(center < s_11)    return false;
    data += 2;
    
    const b_uint8_t s11 = *data;
    if(center < s11)     return false;
    data -= 2 * scoresCols;
    
    const b_uint8_t s1_1 = *data;
    if(center < s1_1)    return false;
    data -= 2;
    
    const b_uint8_t s_1_1 = *data;
    if(center < s_1_1)   return false;
    
    SV_DECLARE(IntContainer, b_int_t);
    IntContainer *delta;
    SV_ALLOC(IntContainer, delta, 4, b_int_t);
    
    /* put together a list of 2d-offsets to where the maximum is also reached */
    if(center == s_1_1)
    {
        SV_PUSH_BACK(delta, -1, b_int_t);
        SV_PUSH_BACK(delta, -1, b_int_t);
    }
    if(center == s0_1)
    {
        SV_PUSH_BACK(delta, 0, b_int_t);
        SV_PUSH_BACK(delta, -1, b_int_t);
    }
    if(center == s1_1)
    {
        SV_PUSH_BACK(delta, 1, b_int_t);
        SV_PUSH_BACK(delta, -1, b_int_t);
    }
    if(center == s_10)
    {
        SV_PUSH_BACK(delta, -1, b_int_t);
        SV_PUSH_BACK(delta, 0, b_int_t);
    }
    if(center == s10)
    {
        SV_PUSH_BACK(delta, 1, b_int_t);
        SV_PUSH_BACK(delta, 0, b_int_t);
    }
    if(center == s_11)
    {
        SV_PUSH_BACK(delta, -1, b_int_t);
        SV_PUSH_BACK(delta, 1, b_int_t);
    }
    if(center == s01)
    {
        SV_PUSH_BACK(delta, 0, b_int_t);
        SV_PUSH_BACK(delta, 1, b_int_t);
    }
    if(center == s11)
    {
        SV_PUSH_BACK(delta, 1, b_int_t);
        SV_PUSH_BACK(delta, 1, b_int_t);
    }
    
    const b_size_t deltaSize = SV_SIZE(delta);
    b_size_t i;
    
    if(deltaSize != 0)
    {
        /* in this case, we have to analyze the situation more carefully:
         * the values are gaussian blurred and then we really decide */
        b_int_t smoothedCenter = 4 * center + 2 * (s_10 + s10 + s0_1 + s01)
        + s_1_1 + s1_1 + s_11 + s11;
        
        for(i = 0; i < deltaSize; i += 2)
        {
            data =
            (scores->pixel + (yLayer - 1 + SV_AT(delta, i + 1))
             * scoresCols + xLayer + SV_AT(delta, i) - 1);
            
            b_int_t otherCenter = *data; data++;
            otherCenter += 2 * (*data);  data++;
            otherCenter += *data;        data += scoresCols;
            otherCenter += 2 * (*data);  data--;
            otherCenter += 4 * (*data);  data--;
            otherCenter += 2 * (*data);  data += scoresCols;
            otherCenter += *data;        data++;
            otherCenter += 2*(*data);    data++;
            otherCenter += *data;
            
            if(otherCenter > smoothedCenter)
            {
                SV_RELEASE(delta);
                return false;
            }
        }
    }
    
    SV_RELEASE(delta);
    
    return true;
}

float _getScoreMaxBelow(const struct BriskScaleSpace *scaleSpace,
                        const b_uint8_t layer,
                        const b_int_t xLayer, const b_int_t yLayer,
                        const b_int_t threshold, bool *isMax,
                        b_float_t *dx, b_float_t *dy)
{
    *isMax = false;
    
    b_float_t x_1, x1, y_1, y1;
    b_int_t x, y;
    
    if(layer % 2 == 0)
    {
        x_1 = (b_float_t)(8 * (xLayer) + 1 - 4) / 6.0f;
        x1  = (b_float_t)(8 * (xLayer) + 1 + 4) / 6.0f;
        y_1 = (b_float_t)(8 * (yLayer) + 1 - 4) / 6.0f;
        y1  = (b_float_t)(8 * (yLayer) + 1 + 4) / 6.0f;
    }
    else
    {
        x_1 = (b_float_t)(6 * (xLayer) + 1 - 3) / 4.0f;
        x1  = (b_float_t)(6 * (xLayer) + 1 + 3) / 4.0f;
        y_1 = (b_float_t)(6 * (yLayer) + 1 - 3) / 4.0f;
        y1  = (b_float_t)(6 * (yLayer) + 1 + 3) / 4.0f;
    }
    
    assert(layer > 0);
    
    struct BriskLayer *layerBelow = &SV_AT(scaleSpace->pyramid, layer - 1);
    
    b_int_t maxX = x_1 + 1;
    b_int_t maxY = y_1 + 1;
    b_float_t tmpMax;
    b_float_t max = getAgastScore(layerBelow, x_1, y_1, 1);
    
    if(max > threshold)
        return 0;
    
    for(x = x_1 + 1; x <= (b_int_t)x1; x++)
    {
        tmpMax = getAgastScore(layerBelow, (b_float_t)x, y_1, 1);
        
        if(tmpMax > threshold)
            return 0;
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = x;
        }
    }
    
    tmpMax = getAgastScore(layerBelow, x1, y_1, 1);
    
    if(tmpMax > threshold)
        return 0;
    
    if(tmpMax > max)
    {
        max = tmpMax;
        maxX = (b_int_t)x1;
    }
    
    /* middle rows */
    for(y = y_1 + 1; y <= (b_int_t)(y1); y++)
    {
        tmpMax = getAgastScore(layerBelow, x_1, (b_float_t)y, 1);
        
        if(tmpMax > threshold)
            return 0;
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = (b_int_t)(x_1 + 1);
            maxY = y;
        }
        for(x = x_1 + 1; x <= (b_int_t)x1; x++)
        {
            tmpMax = getAgastScore(layerBelow, x, y, 1);
            
            if(tmpMax > threshold)
                return 0;
            
            if(tmpMax == max)
            {
                const b_int_t t1 = 2 *
                (getAgastScore(layerBelow, x - 1, y, 1)
                 + getAgastScore(layerBelow, x + 1, y, 1)
                 + getAgastScore(layerBelow, x, y + 1, 1)
                 + getAgastScore(layerBelow, x, y - 1, 1))
                + (getAgastScore(layerBelow, x + 1, y + 1, 1)
                   + getAgastScore(layerBelow, x - 1, y + 1, 1)
                   + getAgastScore(layerBelow, x + 1, y - 1, 1)
                   + getAgastScore(layerBelow, x - 1, y - 1, 1)
                   );
                
                const b_int_t t2 = 2 *
                (getAgastScore(layerBelow, maxX - 1, maxY, 1)
                 + getAgastScore(layerBelow, maxX + 1, maxY, 1)
                 + getAgastScore(layerBelow, maxX, maxY + 1, 1)
                 + getAgastScore(layerBelow, maxX, maxY - 1, 1))
                + (getAgastScore(layerBelow, maxX + 1, maxY + 1, 1)
                   + getAgastScore(layerBelow, maxX - 1, maxY + 1, 1)
                   + getAgastScore(layerBelow, maxX + 1, maxY - 1, 1)
                   + getAgastScore(layerBelow, maxX - 1, maxY - 1, 1)
                   );
                
                if(t1 > t2)
                {
                    maxX = x;
                    maxY = y;
                }
            }
            if(tmpMax > max)
            {
                max = tmpMax;
                maxX = x;
                maxY = y;
            }
        }
        
        tmpMax = getAgastScore(layerBelow, x1, (b_float_t)y, 1);
        
        if(tmpMax > threshold)
            return 0;
        
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = (b_int_t)x1;
            maxY = y;
        }
    }
    
    /* bottom row */
    tmpMax = getAgastScore(layerBelow, x_1, y1, 1);
    if(tmpMax > max)
    {
        max = tmpMax;
        maxX = (b_int_t)(x_1 + 1);
        maxY = (b_int_t)y1;
    }
    for(x = x_1 + 1; x <= (b_int_t)(x1); x++)
    {
        tmpMax = getAgastScore(layerBelow, (b_float_t)x, y1, 1);
        if(tmpMax > max)
        {
            max = tmpMax;
            maxX = x;
            maxY = (b_int_t)y1;
        }
    }
    
    tmpMax = getAgastScore(layerBelow, x1, y1, 1);
    
    if(tmpMax > max)
    {
        max = tmpMax;
        maxX = (b_int_t)x1;
        maxY = (b_int_t)y1;
    }
    
    /*find dx/dy: */
    __CALCULATE_AGAST_SCORE_MATRIX(layerBelow, maxX, maxY);
    
    b_float_t dx1, dy1;
    b_float_t refinedMax = _subpixel2D(s00, s01, s02,
                                       s10, s11, s12,
                                       s20, s21, s22,
                                       &dx1, &dy1);
    
    /* calculate dx/dy in above coordinates */
    b_float_t realX = (b_float_t)maxX + dx1;
    b_float_t realY = (b_float_t)maxY + dy1;
    
    
    if(layer % 2 == 0)
    {
        *dx = (realX * 6.0f + 1.0f) / 8.0f - (b_float_t)xLayer;
        *dy = (realY * 6.0f + 1.0f) / 8.0f - (b_float_t)yLayer;
    }
    else
    {
        *dx = (realX * 4.0f - 1.0f) / 6.0f - (b_float_t)xLayer;
        *dy = (realY * 4.0f - 1.0f) / 6.0f - (b_float_t)yLayer;
    }
    
    bool returnRefined = true;
    
    if(*dx > 1.0f)
    {
        *dx = 1.0f;
        returnRefined = false;
    }
    if(*dx < -1.0f)
    {
        *dx = -1.0f;
        returnRefined = false;
    }
    if(*dy > 1.0f)
    {
        *dy = 1.0f;
        returnRefined = false;
    }
    if(*dy < -1.0f)
    {
        *dy = -1.0f;
        returnRefined = false;
    }
    
    *isMax = true;
    if(returnRefined)
        return fmaxf(refinedMax, max);
    
    return max;
}

#undef __CALCULATE_AGAST_SCORE_MATRIX
